<?php
include_once "library.php";
?>

<!DOCTYPE html>
<head>
    </head>
    <meta charset="UTF-8">
    <title>Postimees Web News</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/jasny-bootstrap.min.css">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">

    <!-- Responsive Style -->
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">

    <!--Fonts-->
    <link rel="stylesheet" media="screen" href="assets/fonts/font-awesome/font-awesome.min.css">

    <!-- Extras -->
    <link rel="stylesheet" type="text/css" href="assets/extras/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/extras/lightbox.css">
    <link rel="stylesheet" type="text/css" href="assets/extras/owl/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="assets/extras/owl/owl.theme.css">
    <link rel="stylesheet" type="text/css" href="assets/extras/owl/owl.transitions.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"></script>
    <script type="text/javascript" src="js/json_parse.js"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script type="text/javascript">
    var news = new Array();
<?php
for ($idx = 0; $idx < count($allNews); $idx++):
$firstNews = $allNews[$idx];
?>
    news[<?php echo $idx; ?>] = '<?php echo json_encode($firstNews); ?>';

<?php endfor;?>

    $('document').ready(function(){
//        console.log(news.length);
        $.each(news , function (index, value){
            var rawJson = value;
            //        console.log(rawJson);

            try{
            var json = rawJson,
                obj = JSON && JSON.parse(json) || $.parseJSON(json);
                console.log(obj);
            } catch (e){
                e.message;
            }
            $str = "";
            $headline = obj.headline;
            $dateCreated = obj.dateCreated;
            $dateModified = obj.dateModified;
            $datePublish = obj.datePublished;
            $isPremium = obj.isPremine;
            $slug = obj.slug;
            $article = obj.articleLead[0].html;
            var $author = obj.thumbnail.sources.landscape.square;
//        $str += "<span>"+$headline+"</span>";
//        $('.section-title').append($str);
//        $('.info').append($article);

            var $fullsection = "";
            $fullsection = "<section id=\"testimonial\">" +
                "<div class=\"container\">" +
                "<div class=\"row\">" +
                "<h1 class=\"section-title wow fadeInLeft animated\" data-wow-delay=\".6s\"><span>" + $headline + "</span></h1>" +
                "<div id=\"testimonial-carousel\" class=\"carousel slide wow fadeInUp animated\" data-ride=\"carousel\" data-wow-delay=\"1.2s\">" +
                "<!-- Indicators -->" +
                "<div class=\"carousel-inner\">" +
                "<div class=\"item active\">" +
                "<blockquote>" +
                "<div class=\"commant\">" +
                "<div class=\"col-lg-3 col-md-3 col-sm-4 col-xs-12\">" +
                "<div class=\"claint\">" +
                "<img src=\"assets/img/testimonial/img"+(index+1)+".jpg\"/>" +
                "</div>" +
                "<span class=\"quote\"><i class=\"fa fa-quote-left\"></i></span>" +
                "</div>" +
                "<div class=\"col-lg-9 col-md-9 col-sm-8 col-xs-12\">" +
                "<div class=\"info\">" + $article +
                "</div></div></div></blockquote></div></div></div></div></div></section>";

            $('#header').after($fullsection);
        })

    });
</script>
</head>

<body>

<div id="header">
    <div class="container">
        <div class="col-md-12 top-header">
            <div class="logo-menu">
                <div class="pull-left wow fadeInDown animated" data-wow-delay=".2">
                    <div id="menu" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
                        <span>menu</span>
                    </div>
                </div>
                <div class="logo pull-left wow fadeInDown animated" data-wow-delay=".2s">
                    <a href="index.html"><img src="assets/img/logo.png" alt="logo"></a>
                </div>
            </div>
            <div class="sidebar-nav">
                <!-- navigation start -->
                <div class="navmenu navmenu-default navmenu-fixed-right offcanvas" style="" id="navigation">
                    <a href="index.html"><img class="logo" src="assets/img/logo.png" alt="logo"></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="banner text-center">
                    <h2 class="wow fadeInDown animated" data-wow-delay=".6s">Aliquam at congue nulla, <br> nunc scelerisque feugiat <br> nibh vel suscipit</h2>
                    <div class="scroll">
                        <a href="#testimonial"><i class="fa fa-angle-down wow fadeInUp animated" data-wow-delay="1.2s"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <div class="copyright wow fadeInUp animated" data-wow-delay=".8s">
                    <p>Copyright &copy; 2016 | All rights reserved.</p>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <div class="scroll-top text-center wow fadeInUp animated" data-wow-delay=".6s">
                    <a href="#header"><i class="fa fa-chevron-circle-up fa-2x"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery Load -->
<script src="assets/js/jquery-min.js"></script>
<!-- Bootstrap JS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- WOW JS plugin for animation -->
<script src="assets/js/wow.js"></script>
<!-- All JS plugin Triggers -->
<script src="assets/js/main.js"></script>
<!-- Smooth scroll -->
<script src="assets/js/smooth-scroll.js"></script>
<!--  -->
<script src="assets/js/jasny-bootstrap.min.js"></script>
<!-- Counterup -->
<script src="assets/js/jquery.counterup.min.js"></script>
<!-- waypoints -->
<script src="assets/js/waypoints.min.js"></script>
<!-- circle progress -->
<script src="assets/js/circle-progress.js"></script>
<!-- owl carousel -->
<script src="assets/js/owl.carousel.js"></script>
<!-- lightbox -->
<script src="assets/js/lightbox.min.js"></script>

</body>
</html>